var express = require('express');
var app = express();

app.use(express.static(__dirname + '/app'));
app.get('*', function (req, res) {
    res.redirect('.#' + req.originalUrl);
});

var server = app.listen(process.env.PORT || 3000, function () {
    console.log('Node app is running');
});

