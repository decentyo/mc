define([
    'backbone'
], function (Backbone) {
    return Backbone.View.extend({
        render: function () {
            this.$el.html('<ul><li>Dummy</li><li>List</li></ul>');
            return this;
        }
    })
});
