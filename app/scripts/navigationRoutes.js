define([
    'underscore',
    'json!../data/tabs.json'
], function (_, tabs) {
    'use strict';

    var NavigationRoutes = {};

    var tabs = _.sortBy(tabs, 'order');

    tabs.forEach(function (tab) {
        NavigationRoutes[tab.title] = {
            route: tab.id,
            path: tab.path
        };
    });

    NavigationRoutes['default'] = {
        route: '*default',
        redirect: tabs[0].title,
        hide: true
    };

    return NavigationRoutes;
});

