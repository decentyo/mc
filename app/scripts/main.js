/*global require*/
'use strict';

require.config({
    shim: {},
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/lodash/dist/lodash',
        text: '../bower_components/requirejs-plugins/lib/text',
        json: '../bower_components/requirejs-plugins/src/json'
    }
});

require([
    'jquery',
    'backbone',
    'routes/appRouter',
    'views/navbar',
    'views/content',
    'navigationRoutes'
], function ($, Backbone, AppRouter, Navbar, Content, NavigationRoutes) {
    var appRouter = new AppRouter();
    new Navbar({el: $('#nav-item-container'), routes: NavigationRoutes});
    new Content({el: $('#container'), routes: NavigationRoutes});

    Backbone.history.start({pushState: true});
});
