define([
    'backbone',
    'navigationRoutes'
], function (Backbone, NavigationRoutes) {
    'use strict';

    return Backbone.Router.extend({
        initialize: function () {
            this.route(NavigationRoutes['default'].route, 'default');

            for (var key in NavigationRoutes) {
                if (key !== 'default') {
                    this.route(NavigationRoutes[key].route, key);
                }
            }
        },

        routes: {}
    });
});
