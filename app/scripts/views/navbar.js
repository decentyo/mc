define([
    'jquery',
    'backbone',
    'underscore'
], function ($, Backbone, _) {
    'use strict';

    return Backbone.View.extend({
        initialize: function (options) {

            // we pass the NavigationRoutes as an option, later we traverse the routes in render function.
            this.routes = options.routes;

            // router will fire route event whenever url changes, we rerender our view
            // on each url change
            Backbone.history.on('route', function (source, path) {
                this.render(path);
            }, this);
        },

        // events hash that handles events in our view
        events: {
            // click handler for a navigation tab
            'click a': function (source) {
                var hrefRslt = source.target.getAttribute('href');
                Backbone.history.navigate(hrefRslt, {trigger: true});

                // Cancel the regular event handing so that we won't actually
                // change URLs We are letting Backbone handle routing
                return false;
            }
        },

        render: function (route) {
            // Clear the view element
            this.$el.empty();

            // this is the underscore template for a navigation tab.
            var template = _.template("<li class='<%=active%>'><a href='<%= url%>'><%=visible%></a></li>");

            if (route === 'default') {
                route = this.routes[route].redirect;
            }

            // Traverse the NavigationRoutes
            for (var key in this.routes) {
                // don't render if route is hidden
                if (!this.routes[key].hide) {
                    this.$el.append(template({
                        url: this.routes[key].route,
                        visible: key,
                        active: route === key ? 'active' : ''
                    }));
                }
            }
        }
    });
});


