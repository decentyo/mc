define([
    'backbone'
], function (Backbone) {
    'use strict';

    return Backbone.View.extend({
        initialize: function (options) {
            // pass the NavigationRoutes as an option
            this.routes = options.routes;

            // Listen for route changes, and render when a route changes.
            Backbone.history.on('route', function (source, path) {
                this.render(path);
            }, this);
        },

        render: function (route) {
            var that = this;

            if (route === 'default') {
                route = this.routes[route].redirect;
            }

            var path = this.routes[route].path;
            require([path], function (View) {
                if (View) {
                    that.$el.html((new View()).render().el);
                }
            });
        }
    });
});
